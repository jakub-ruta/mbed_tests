#Úloha 3 - Práce s Thread 1 

Pro práci s displejem jsem zvolil knihovnu LCD_DISCO_F746NG, která poskytuje lepší interface pro základní stejno jmenou knihovnu, která obsahuje více součástí

<br />

Vytvořil jsem program o 5 vláknech, první 3 vlákna píší na displej, další 3 obsluhují RGB LED.

Pro displej jsem musel napsat část na jeho inicializaci a poté na zobrazování textu (zvolil jsem možnst psaní textu pod sebe, poté posunutí doporstřed a nakonec doprava. Tím je přesně vidět, jak procesy nezávisle běží. Stejně je to vidět  na změně barvy LEDky.


Došel jsem k závěru, že pokud v mainu nebude while(true){}, program se po prvním běhu zastaví a ukončí, pokud je založeno jiné vlákno, ani se nevykoná. Vždy tedy asi musí být na konci mainu while(true){}

