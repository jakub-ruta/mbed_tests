#include "mbed.h"
#include "LCD_DISCO_F746NG.h"

//definition of text
char text_test_0[] = "Task 0";
char text_test_1[] = "Task 1";
char text_test_2[] = "Task 2";

// prepare functions
void writeToDisplay(char text[], int line);
void clearLine(int line);
void InitScreen(void);
void thread0_code(void);
void thread1_code(void);
void thread2_code(void);
void thread3_code(void);
void thread4_code(void);
void thread5_code(void);

//definition of display

LCD_DISCO_F746NG lcd;

//definiton if threads
Thread thread0;
Thread thread1;
Thread thread2;
Thread thread3;
Thread thread4;
Thread thread5;

//definition of output pins
DigitalOut led1(LED1);
DigitalOut led2(D0);
DigitalOut led3(D1);
DigitalOut led4(D2);

//needed global variables
int line = 1;


//main program
int main(){    
    InitScreen();  
    writeToDisplay("Start", 0);  
    thread0.start(thread0_code);
    thread1.start(thread1_code);
    thread2.start(thread2_code);
    thread3.start(thread3_code);
    thread4.start(thread4_code);
    thread5.start(thread5_code);
    while(true){
    }  
    
}
// init screen with black blue color and greeen text
void InitScreen(){
    lcd.Init();
    lcd.SetBackColor(LCD_COLOR_BLUE);
    lcd.SetTextColor(LCD_COLOR_GREEN);
}

void writeToDisplay(char text[], int line_loc){
    if (line_loc < 11) {
        lcd.DisplayStringAt(0, LINE(line_loc), (uint8_t *)text, LEFT_MODE);
    }
    else if (line_loc < 22){            
        lcd.DisplayStringAt(0, LINE(line_loc - 11), (uint8_t *)text, CENTER_MODE);
    }
    else if (line_loc < 33){            
        lcd.DisplayStringAt(0, LINE(line_loc - 22), (uint8_t *)text, RIGHT_MODE);
    }
    else{
         lcd.Clear(LCD_COLOR_BLACK);
         lcd.DisplayStringAt(0, LINE(0), (uint8_t *)text, LEFT_MODE);   
         line = 0;
    }

}

void clearLine(int line_loc){
    lcd.ClearStringLine(line_loc);
}

void thread0_code(){
    while (true) {
        writeToDisplay(text_test_0, line);
        line ++;
        ThisThread::sleep_for(900);
    }
}

void thread1_code(){
    while (true) {
        writeToDisplay(text_test_1, line);
        line ++;
        ThisThread::sleep_for(1300);
    }
}

void thread2_code(){
    while (true) {
        writeToDisplay(text_test_2, line);
        line ++;
        ThisThread::sleep_for(3200);     
    }
}

void thread3_code(){
    while (true) {
        led3 = !led3;
        thread_sleep_for(2250);
   
    }
}

void thread4_code(){
    while (true) {
        led1 = !led1;
        led2 = !led2;
        ThisThread::sleep_for(1500);     
    }
}
void thread5_code(){
    while (true) {
        led4 = !led4;        
        ThisThread::sleep_for(3500);     
    }
}