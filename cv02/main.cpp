#include "mbed.h"
#include "LCD_DISCO_F746NG.h"

char text_name[] = "Jakub Ruta";
char text_test[] = "Test displaye";

LCD_DISCO_F746NG lcd;

DigitalOut led1(LED1);


void writeToDisplay(char text[], int line){
    lcd.DisplayStringAt(0, LINE(line), (uint8_t *)text, CENTER_MODE);
    }

int main()
{  
    
    lcd.SetBackColor(LCD_COLOR_BLUE);
    
    writeToDisplay(text_test, 1);
    writeToDisplay(text_name, 5);
  
   
}



