#include "mbed.h"
#include <inttypes.h>

#if !defined(MBED_THREAD_STATS_ENABLED)
#error "Thread statistics not enabled"
#endif
 

#define MAX_THREAD_INFO 10

#define MAX_THREAD_STATS    0x8
#define BLINKY_THREAD_STACK 224
#define IDLE_THREAD_STACK 384
#define STOP_FLAG 0xEF
#define WAIT_TIME_MS 500
//definition of text
char text_debug_button_0[] = "Button pushed";
char text_debug_button_1[] = "Button pulled";
char text_debug_run[] = "I am running!";

//prepare functions
void thread0_code(void);
void thread1_code(void);
void printStats(void);

//definiton if threads
Thread thread0;
Thread thread1;

//definition of output pins
DigitalIn button(USER_BUTTON);

//needed global variables

mbed_stats_heap_t heap_info;
mbed_stats_stack_t stack_info[ MAX_THREAD_INFO ];

//main program
int main(){ 
    button.mode(PullDown); 
    thread0.start(thread0_code);
    thread1.start(thread1_code);
    
    
    
    
    while(true){
    }  
    
}


//code for thred 0
void thread0_code(){
    bool lastState = false; // variable for store if button was pressed
    while(true){ // infinite loop for never end thread
        if (button.read() && !lastState){ // is button pressed - simulate Raise edge
            printf(text_debug_button_0);
            printf("\n"); // only \n - defined by Serial monitor - should be \r\n on others
            lastState = true;
        }else if (!button.read() && lastState){ // is button not pressed - simulate Falling edge
            lastState = false;
            printf(text_debug_button_1);
            printf("\n");
            printStats();
            }
    }   
}

//code for thred 1
void thread1_code(){
    
    while(true){ // infinite loop for never end thread
    printf(text_debug_run);
    printf("\r\n");
    ThisThread::sleep_for(1000);
    }   
}

void printStats(){
    debug("\nThis message is from memory debug function");
    debug_if(1, "\nThis message is from debug_if function");
    debug_if(0, "\nSOMETHING WRONG!!! This message from debug_if function shouldn't show on bash");

    printf("\nMemoryStats:");
    mbed_stats_heap_get(&heap_info);
    printf("\n\tBytes allocated currently: %ld", heap_info.current_size);
    printf("\n\tMax bytes allocated at a given time: %ld", heap_info.max_size);
    printf("\n\tCumulative sum of bytes ever allocated: %ld", heap_info.total_size);
    printf("\n\tCurrent number of bytes allocated for the heap: %ld", heap_info.reserved_size);
    printf("\n\tCurrent number of allocations: %ld", heap_info.alloc_cnt);
    printf("\n\tNumber of failed allocations: %ld", heap_info.alloc_fail_cnt);

    mbed_stats_stack_get(&stack_info[0]);
    printf("\nCumulative Stack Info:");
    printf("\n\tMaximum number of bytes used on the stack: %ld", stack_info[0].max_size);
    printf("\n\tCurrent number of bytes allocated for the stack: %ld", stack_info[0].reserved_size);
    printf("\n\tNumber of stacks stats accumulated in the structure: %ld", stack_info[0].stack_cnt);

    mbed_stats_stack_get_each(stack_info, MAX_THREAD_INFO);
    printf("\nThread Stack Info:");
    for (int i = 0; i < MAX_THREAD_INFO; i++) {
        if (stack_info[i].thread_id != 0) {
            printf("\n\tThread: %d", i);
            printf("\n\t\tThread Id: 0x%08lX", stack_info[i].thread_id);
            printf("\n\t\tMaximum number of bytes used on the stack: %ld", stack_info[i].max_size);
            printf("\n\t\tCurrent number of bytes allocated for the stack: %ld", stack_info[i].reserved_size);
            printf("\n\t\tNumber of stacks stats accumulated in the structure: %ld", stack_info[i].stack_cnt);
        }
    }

    printf("\nDone...\n\n");
    
    printf("Thread stats");
      mbed_stats_thread_t *stats = new mbed_stats_thread_t[MAX_THREAD_STATS];
    int count = mbed_stats_thread_get_each(stats, MAX_THREAD_STATS);

    for (int i = 0; i < count; i++) {
        printf("ID: 0x%" PRIx32 "\n", stats[i].id);
        printf("Name: %s \n", stats[i].name);
        printf("State: %" PRId32 "\n", stats[i].state);
        printf("Priority: %" PRId32 "\n", stats[i].priority);
        printf("Stack Size: %" PRId32 "\n", stats[i].stack_size);
        printf("Stack Space: %" PRId32 "\n", stats[i].stack_space);
        printf("\n");
    }
    
    printf("END of threas stats\n\n");
}

