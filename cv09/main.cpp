#include "mbed.h"
#include "LCD_DISCO_F746NG.h"
#include "EthernetInterface.h"
#include <string>

//definition of text
char text_connecting[] = "Connecting";
char text_done[] = "Sended";



//variables
const std::string api_key = "7BGKHY6ER3G9WR3M";
const std::string student_num = "M18000093";


// prepare functions
void writeToDisplay(char text[], int line);
void clearLine(int line);
void InitScreen(void);

void thread0_code(void);
void create_thread_for_data(void);
void make_request(void);





//definition of display

LCD_DISCO_F746NG lcd;


//definiton if threads
Thread thread0; 




//definition of input pins
InterruptIn btn(USER_BUTTON);


//needed global variables
int line = 1;
EthernetInterface eth;



//main program
int main(){ 
    InitScreen();  
    set_time(1611011338);
   
    btn.rise(create_thread_for_data);
    
    while(true){
    }  
    
}

void create_thread_for_data(){
    thread0.start(make_request);
    thread0.join(); 

}
// init screen with black blue color and greeen text
void InitScreen(){
    lcd.Init();
    lcd.DisplayOn();
    
}



void clearLine(int line_loc){
    lcd.ClearStringLine(line_loc);
}

void thread0_code(){    
    

}   

void writeToDisplay(char text[], int line_loc){
    if (line_loc < 11) {
        lcd.DisplayStringAt(0, LINE(line_loc), (uint8_t *)text, LEFT_MODE);
    }
    else if (line_loc < 22){            
        lcd.DisplayStringAt(0, LINE(line_loc - 11), (uint8_t *)text, CENTER_MODE);
    }
    else if (line_loc < 33){            
        lcd.DisplayStringAt(0, LINE(line_loc - 22), (uint8_t *)text, RIGHT_MODE);
    }
    else{
         lcd.Clear(LCD_COLOR_BLACK);
         lcd.DisplayStringAt(0, LINE(0), (uint8_t *)text, LEFT_MODE);   
         line = 0;
    }

}
void make_request(){
    clearLine(5);
    writeToDisplay(text_connecting, 5);
    nsapi_error_t status;
    eth.connect();
    SocketAddress address;
    eth.get_ip_address(&address);
    eth.gethostbyname("api.thingspeak.com", &address);    
    TCPSocket socket;    
    socket.open(&eth);
    address.set_port(80);
    socket.connect(address);
    time_t field_time = time(NULL);
    char time_buff[32];
    strftime(time_buff, 32, "%Y-%m-%d %H:%M:%S\n", localtime(&field_time));
    std::string req_payload = api_key + "&field1=" + student_num + "&field2=" + time_buff + "\r\n\r\n";
    std::string req = "POST /update HTTP/1.1\nHost: api.thingspeak.com\nConnection: close\nX-THINGSPEAKAPIKEY: " + api_key + "\nContent-Type: application/x-www-form-urlencoded\nContent-Length: " + to_string(req_payload.length()) + "\n\n" + req_payload;
    
    socket.send(req.c_str(), sizeof(req.c_str()));
    
    
    //close communication
    socket.close();
    eth.disconnect();
    clearLine(5);
    writeToDisplay(text_done, 5);
    
    return;
}







