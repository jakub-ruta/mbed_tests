#include "mbed.h"
#include "LCD_DISCO_F746NG.h"
#include "ResetReason.h"
#include <string>
#include "rtos.h"

//definition of text
char text_reset_reason_1[] = "Power on";


//variables
#define WATCHDOGTIME 5000

// prepare functions
void writeToDisplay(char text[], int line);
void clearLine(int line);
void InitScreen(void);
void light(void);
void dark(void);



//definition of display

LCD_DISCO_F746NG lcd;



//definiton of threads
Thread thread0; //queue thread



//definition of input pins
InterruptIn btn(USER_BUTTON);


//needed global variables
int line = 1;

//event queue

EventQueue queue(32 * EVENTS_EVENT_SIZE);


//main program
int main(){ 
    
        
    InitScreen();  
    
    thread0.start(callback(&queue, &EventQueue::dispatch_forever));
    btn.rise(queue.event(light));
    
    btn.fall(queue.event(dark));    
    while(true){
    }  
    
}
// init screen with black blue color and greeen text
void InitScreen(){
    lcd.Init();
    lcd.SetBackColor(LCD_COLOR_BLUE);
    lcd.SetTextColor(LCD_COLOR_GREEN);
}

void writeToDisplay(char text[], int line_loc){
    if (line_loc < 11) {
        lcd.DisplayStringAt(0, LINE(line_loc), (uint8_t *)text, LEFT_MODE);
    }
    else if (line_loc < 22){            
        lcd.DisplayStringAt(0, LINE(line_loc - 11), (uint8_t *)text, CENTER_MODE);
    }
    else if (line_loc < 33){            
        lcd.DisplayStringAt(0, LINE(line_loc - 22), (uint8_t *)text, RIGHT_MODE);
    }
    else{
         lcd.Clear(LCD_COLOR_BLACK);
         lcd.DisplayStringAt(0, LINE(0), (uint8_t *)text, LEFT_MODE);   
         line = 0;
    }

}

void clearLine(int line_loc){
    lcd.ClearStringLine(line_loc);
}

void dark(void){
    
    lcd.SetBackColor(LCD_COLOR_BLUE);
    
}
void light(void){
    lcd.SetBackColor(LCD_COLOR_RED);
}