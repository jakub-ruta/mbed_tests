#include "mbed.h"

//definition of text
char text_debug_button_0[] = "Button pushed";
char text_debug_button_1[] = "Button pulled";

//prepare functions
void thread0_code(void);

//definiton if threads
Thread thread0;

//definition of output pins
DigitalIn button(USER_BUTTON);

//needed global variables

//main program
int main(){ 
    button.mode(PullDown); 
    thread0.start(thread0_code);
    while(true){
    }  
    
}


//code for thred 0
void thread0_code(){
    bool lastState = false; // variable for store if button was pressed
    while(true){ // infinite loop for never end thread
        if (button.read() && !lastState){ // is button pressed - simulate Raise edge
            printf(text_debug_button_0);
            printf("\n"); // only \n - defined by Serial monitor - should be \r\n on others
            lastState = true;
        }else if (!button.read() && lastState){ // is button not pressed - simulate Falling edge
            lastState = false;
            printf(text_debug_button_1);
            printf("\n");
            }
    }   
}

