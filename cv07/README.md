# Cvičení 7, Příčina resetu

Dle zadání jsem na displej vypsal příčinu posledního resetu. Tedy zda šlo o reset watchdogem, tlačítekem reset, výpadkem napájení nebo softwarově vyvolaným.

Pro komunikaci na sériovém portu jsem zase musel vybrat rychlost 9600 baudů. Z PC jsem  pak poslal "R" jako reset, který kód zpracoval a resetoval desku.

Pro výpis na displej jsem použil strukturu, kterou jsem si vytvořil v předcházejících projektech.