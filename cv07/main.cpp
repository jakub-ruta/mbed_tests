#include "mbed.h"
#include "LCD_DISCO_F746NG.h"
#include "ResetReason.h"
#include <string>

//definition of text
char text_reset_reason_1[] = "Power on";
char text_reset_reason_2[] = "Hardware Pin";
char text_reset_reason_3[] = "Software Reset";
char text_reset_reason_4[] = "Watchdog";
char text_reset_reason_0[] = "Unknown";

//variables
#define WATCHDOGTIME 5000

// prepare functions
void writeToDisplay(char text[], int line);
void clearLine(int line);
void InitScreen(void);
void thread0_code(void);
int reset_reason_to_int(const reset_reason_t reason);
void print_reset_reason_to_display(int reason);


//definition of display

LCD_DISCO_F746NG lcd;

//definiton of serial port
static BufferedSerial pc(USBTX, USBRX); // tx, rx

//definiton if threads
Thread thread0;
Thread thread1;
Thread thread2;


//definition of input pins
DigitalIn button(USER_BUTTON);


//needed global variables
int line = 1;


//main program
int main(){ 
    Watchdog &watchdog = Watchdog::get_instance();
    watchdog.start(WATCHDOGTIME);   
    const reset_reason_t reason = ResetReason::get();
        
    InitScreen();  
    print_reset_reason_to_display(reset_reason_to_int(reason));
    //writeToDisplay("Start", 0);  
    thread0.start(thread0_code);
    

    
    while(true){
    }  
    
}
// init screen with black blue color and greeen text
void InitScreen(){
    lcd.Init();
    lcd.SetBackColor(LCD_COLOR_BLUE);
    lcd.SetTextColor(LCD_COLOR_GREEN);
}

void writeToDisplay(char text[], int line_loc){
    if (line_loc < 11) {
        lcd.DisplayStringAt(0, LINE(line_loc), (uint8_t *)text, LEFT_MODE);
    }
    else if (line_loc < 22){            
        lcd.DisplayStringAt(0, LINE(line_loc - 11), (uint8_t *)text, CENTER_MODE);
    }
    else if (line_loc < 33){            
        lcd.DisplayStringAt(0, LINE(line_loc - 22), (uint8_t *)text, RIGHT_MODE);
    }
    else{
         lcd.Clear(LCD_COLOR_BLACK);
         lcd.DisplayStringAt(0, LINE(0), (uint8_t *)text, LEFT_MODE);   
         line = 0;
    }

}

void clearLine(int line_loc){
    lcd.ClearStringLine(line_loc);
}

void thread0_code(){
    char *c = new char[1];
    while(true){
        pc.read(c, sizeof(c));
        if (*c == 'R'){
            NVIC_SystemReset();
        }
    }
}   





int reset_reason_to_int(const reset_reason_t reason)
{
    switch (reason) {
        case RESET_REASON_POWER_ON:
            return 1;
        case RESET_REASON_PIN_RESET:
            return 2;
        case RESET_REASON_SOFTWARE:
            return 3;
        case RESET_REASON_WATCHDOG:
            return 4;
        default:
            return 0;
    }
}

void print_reset_reason_to_display(int reason){
    switch(reason){
        case 1:
            writeToDisplay(text_reset_reason_1, 5);
            break;
        case 2:
            writeToDisplay(text_reset_reason_2, 5);
            break;
        case 3:
            writeToDisplay(text_reset_reason_3, 5);
            break;
        case 4:
            writeToDisplay(text_reset_reason_4, 5);
            break;
        case 0:
            writeToDisplay(text_reset_reason_0, 5);
            break;
    }
}
