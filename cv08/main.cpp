#include "mbed.h"
#include "LCD_DISCO_F746NG.h"
#include "stm32746g_discovery_ts.h"
#include <string>

//definition of text
char text_time_plus[] = "+10";
char text_time_minus[] = "-10";
char text_time_set[] = "set";

//define buttons position
#define BUTTON_height 50
#define BUTTON_weight 70
#define BUTTON_1 20
#define BUTTON_2 200
#define BUTTON_3 400
#define BUTTON_y 200


//variables
#define WATCHDOGTIME 5000

// prepare functions
void writeToDisplay(char text[], int line);
void clearLine(int line);
void InitScreen(void);
void draw_controls(void);
void thread0_code(void);
void print_time(void);
void adjust_time(int time);
void handle_touch(int x, int y , int x_last, int y_last);
void handle_tictac(void);
void start_tick(bool enable);
void boom(void);
void restart(void);




//definition of display

LCD_DISCO_F746NG lcd;


//definiton if threads
Thread thread0; 




//definition of input pins
InterruptIn restart_button(USER_BUTTON);


//needed global variables
unsigned int timer_time_1 = 120;
LowPowerTicker tictac;



//main program
int main(){ 
    InitScreen();  
    draw_controls();
    
    thread0.start(thread0_code); //touchscreen   
    restart_button.rise(restart);
    
    while(true){
    }  
    
}
// init screen with black blue color and greeen text
void InitScreen(){
    lcd.Init();
    lcd.DisplayOn();
    draw_controls();
}

void draw_controls(){
    lcd.Clear(LCD_COLOR_BLACK);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.DrawRect(BUTTON_1, BUTTON_y, BUTTON_weight, BUTTON_height);    
    lcd.DrawRect(BUTTON_2, BUTTON_y, BUTTON_weight, BUTTON_height);
    lcd.DrawRect(BUTTON_3, BUTTON_y, BUTTON_weight, BUTTON_height);
    lcd.DisplayStringAt(BUTTON_1+10, BUTTON_y+15, (uint8_t *)text_time_minus, LEFT_MODE);    
    lcd.DisplayStringAt(BUTTON_2+10, BUTTON_y+15, (uint8_t *)text_time_set, LEFT_MODE);
    lcd.DisplayStringAt(BUTTON_3+10, BUTTON_y+15, (uint8_t *)text_time_plus, LEFT_MODE);
    print_time();
}
void handle_tictac(void){
    if (timer_time_1 <= 0){
        boom();
    }else{
        timer_time_1--;
        print_time();
    }   
}

void handle_touch(int x, int y , int x_last, int y_last){
    if(x != x_last && y != y_last){
        if(x >= BUTTON_1 && x <= BUTTON_1 + BUTTON_weight && y >= BUTTON_y && y <= BUTTON_y + BUTTON_height){
            adjust_time(-10);
        }
        else if(x >= BUTTON_2 && x <= BUTTON_2 + BUTTON_weight && y >= BUTTON_y && y <= BUTTON_y + BUTTON_height){
            start_tick(true);
        }
        else if(x >= BUTTON_3 && x <= BUTTON_3 + BUTTON_weight && y >= BUTTON_y && y <= BUTTON_y + BUTTON_height){
            adjust_time(10);
        }        
    }
}

void start_tick(bool enable){
    if (enable){
        tictac.attach(handle_tictac, 1s);
    }else{
        tictac.detach();
    }
}



void adjust_time(int time){
    timer_time_1 += time;
    print_time();
}

void boom(){
    lcd.Clear(LCD_COLOR_RED);
    lcd.SetBackColor(LCD_COLOR_RED);
}       



void clearLine(int line_loc){
    lcd.ClearStringLine(line_loc);
}

void thread0_code(){    
    TS_StateTypeDef touchscreen_state;
    BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    int x, y, x_last, y_last;
    while(true){
        BSP_TS_GetState(&touchscreen_state);
        if(touchscreen_state.touchDetected){
            x = touchscreen_state.touchX[0];
            y = touchscreen_state.touchY[0];
            handle_touch(x, y , x_last, y_last);
            x_last = x;
            y_last = y;
        }else{
            x_last = 0;
            y_last = 0; 
        }
    }

}   


void print_time(){
    clearLine(6);
    int min = timer_time_1 / 60;
    int sec = (timer_time_1 % 60);
    char text[6];
    sprintf(text, "%02d:%02d", min, sec);
    lcd.DisplayStringAt(0, LINE(6), (uint8_t *)text, CENTER_MODE);    
}
void restart(){
    start_tick(false);
    timer_time_1 = 120;
    InitScreen();  
    draw_controls();
}




