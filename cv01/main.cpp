#include "mbed.h"

DigitalOut myled(LED1);

int main() {  
    while(1) {
        for (int i = 0; i < 10; i++){
            myled = 1; 
            wait(0.3); 
            myled = 0; 
            wait(0.3); 
        }
        myled = 1; 
        wait(3); 
        myled = 0; 
        wait(1.5); 
    }
}
